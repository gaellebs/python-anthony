from tkinter import messagebox
import random
from tkinter import *
from tkinter import ttk
import sqlite3
import home
import reg
import menu

def root():
    global login_root
    login_root = Tk()
    Login(login_root)
    login_root.mainloop()

class Login():
    def __init__(self, master8):
        master8.title("Login Page")
        master8.geometry("500x300")
        master8.resizable(False, False)
        self.user = Entry(master8)
        self.user.place(x=110, y=130)
        self.passw = Entry(master8)
        self.passw.place(x=250, y=130)
        self.label = Label(master8, text="Username")
        self.label.place(x=110, y=105)
        self.label2 = Label(master8, text="Password")
        self.label2.place(x=250, y=105)
        self.button1 = Button(master8, text="Enter",command = self.log)
        self.button1.place(x=220, y=160)

    def log(self):
        user = self.user.get()
        passw = self.passw.get()
        conn = sqlite3.connect("database.db")
        c = conn.cursor()
        c.execute("""CREATE TABLE IF NOT EXISTS user(
                    userID INTEGER PRIMARY KEY AUTOINCREMENT,
                    user VARCHAR(15),
                    passw VARCHAR(15)
                    )""")
        conn.commit()
        input = [(user), (passw)]
        c.execute("""SELECT user,passw FROM user WHERE user=(?) AND passw=(?)""", input)
        check = c.fetchall()
        if check != []:
            login_root.destroy()
            home.close()
            menu.root()
        else:
            messagebox.showerror(title="Error", message="User not found")
            login_root.destroy()
