from tkinter import messagebox
import random
from tkinter import *
from tkinter import ttk
import sqlite3
import home
import log
import reg

def root():
    global home_root
    home_root = Tk()
    Home(home_root)
    home_root.mainloop()

class Home():
    def __init__(self, master6):
        master6.title("Home")
        master6.geometry("400x300")
        self.button1 = Button(master6, text="Login",command=self.login)
        self.button1.place(x=210, y=135)
        self.button2 = Button(master6, text="Register",command=self.reg)
        self.button2.place(x=140, y=135)

    def login(self):
        log.root()

    def reg(self):
        reg.root()
def close():
    home_root.destroy()