from tkinter import messagebox
import random
from tkinter import *
from tkinter import ttk
import sqlite3
import home
import reg

def root():
    global reg_root
    reg_root = Tk()
    Register(reg_root)
    reg_root.mainloop()

class Register():
    def __init__(self, master7):
        master7.title("Registration Page")
        master7.geometry("500x300")
        master7.resizable(False, False)
        self.user = Entry(master7)
        self.user.place(x=110, y=130)
        self.passw = Entry(master7)
        self.passw.place(x=250, y=130)
        self.label = Label(master7, text="Username")
        self.label.place(x=110, y=105)
        self.label2 = Label(master7, text="Password")
        self.label2.place(x=250, y=105)
        self.button1 = Button(master7, text="Enter",command=self.regent)
        self.button1.place(x=220, y=160)

    def regent(self):
        user = self.user.get()
        passw = self.passw.get()
        if user == "" or passw == "":
            messagebox.showerror(title="Error", message="Input Error")
        else:
            conn = sqlite3.connect("database.db")
            c = conn.cursor()
            c.execute("""CREATE TABLE IF NOT EXISTS user(
            userID INTEGER PRIMARY KEY AUTOINCREMENT,
            user VARCHAR(15),
            passw VARCHAR(15)
            )""")
            conn.commit()
            argument = [(user)]
            c.execute("""SELECT user FROM user WHERE user=(?)""", argument)
            users = c.fetchall()
            if users != []:
                messagebox.showerror(title="Error", message="Username already registered")
                conn.close()


            else:
                info = [(user, passw)]
                c.executemany("""INSERT INTO user (user,passw) VALUES (?,?)""", info)
                conn.commit()
                reg_root.destroy()
                messagebox.showinfo(title="Message", message="User Created")

