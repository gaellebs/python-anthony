from tkinter import messagebox
import random
from tkinter import *
from tkinter import ttk
import sqlite3
import account
import home
import log
import reg
import horse
import blackjack
import roulette

def root():
    global men_root
    men_root = Tk()
    Menu(men_root)
    men_root.mainloop()


class Menu():
    def __init__(self, master):
        self.master = master
        self.master.title("Menu")
        self.master.resizable(False, False)
        self.master.geometry("800x500")
        self.back = PhotoImage(file="backmain.png")
        self.label = Label(master,image=self.back)
        self.label.place(x=0,y=1)
        self.button1 = Button(master, text="Blackjack", command=self.bj)
        self.button1.place(x=275 , y=250)
        self.button2 = Button(master, text="Roulette", command=self.roulette)
        self.button2.place(x=350 , y=250)
        self.button3 = Button(master, text="Horse Racing", command=self.horse)
        self.button3.place(x=425 , y=250)
        self.button4 = Button(master, text="Account", command=self.acc)
        self.button4.place(x=740, y=5)


    def bj(self):
        men_root.destroy()
        blackjack.root()

    def roulette(self):
        men_root.destroy()
        roulette.root()

    def horse(self):
        men_root.destroy()
        horse.root()


    def acc(self):
        men_root.destroy()
        account.root()





